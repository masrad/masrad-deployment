# About

MASRAD:platform is a platform for organizing, archiving and availing oral history collections. It seeks to respond to the scarcity of tools available for archiving oral history collections, and their rigidity in imposing certain epistemic decisions on archivists. It provides the technical tools to help archivists through the complete process of archiving their oral history collections, starting with uploading a file, through cataloguing, indexing, transcription, segmentation, anonymisation, and finally, publishing.

MASRAD:platform is a project of the [Masrad Collective](https://masrad.org/). It is supported by the [Bassel Khartabil Free Culture Fellowship](https://basselkhartabil.org/) and [Dawlaty](https://dawlaty.org).

### This project is a work in progress under active development!

This is repository is a dockerised version of the MASRAD:platform. It deploys the [backend](https://gitlab.com/masrad/masrad-backend) and the [frontend](https://gitlab.com/masrad/masrad-frontend), with all the services that they require. 

# Usage

0. Make sure that you have [`docker`](https://docs.docker.com/install/#server) and [`docker-compose`](https://docs.docker.com/compose/install/) installed 
1. Clone this repo  
2. Copy the `.env` file making the appropriate modifications 
3. Run `docker-compose up`, then go to address defined in your `.env` file (`127.0.0.1` by default)

If you want SSL (you definitely should on production), you can get a certificate from Let's Encrypt. 

1. In your .env file, comment out or remove the `NO_SSL=#` line
2. Obtain a certificate from Let's Encrypt, using certbot. Run the following command, replacing `exmaple.com` with your own domain.

```
sudo docker run -it --rm --name certbot \
            -v "/etc/letsencrypt:/etc/letsencrypt" \
            -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
            -p 80:80 certbot/certbot certonly \
            --standalone -d example.com
```

That's it!