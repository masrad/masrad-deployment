import os

DEBUG = True

ALLOWED_HOSTS = [os.environ['SERVER_URL'], ]

ADMINS = ((os.environ['ADMINS_NAME'], os.environ['ADMINS_EMAIL']),)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
SERVER_EMAIL = os.environ['SERVER_EMAIL']
EMAIL_HOST = os.environ['EMAIL_HOST']
EMAIL_HOST_USER = os.environ['EMAIL_HOST_USER']
EMAIL_HOST_PASSWORD = os.environ['EMAIL_HOST_PASSWORD']

CORS_ORIGIN_ALLOW_ALL = False

MEDIA_ROOT = os.path.join('/home', 'media')
SENDFILE_ROOT = os.path.join(MEDIA_ROOT, 'protected')
SENDFILE_URL = '/protected'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, 'masrad.debug.log'),
        },
        'media_access_file': {
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, 'media_access.log'),
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'media_access': {
            'handlers': ['media_access_file'],
            'level': 'INFO',
            'propagate': False,
        },
    },
}
