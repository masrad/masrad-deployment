python manage.py migrate
python manage.py collectstatic  --noinput

python manage.py compilemessages -v3 -l ar

gunicorn -b 0.0.0.0:8000 --daemon --log-file=/var/log/gunicorn.log --log-level debug backend.wsgi:application 


nginx -g "daemon off;"